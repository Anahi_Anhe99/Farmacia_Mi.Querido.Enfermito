﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Validadores;
using Farmacia.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Farmacia.GUI
{
    /// <summary>
    /// Lógica de interacción para Acceso.xaml
    /// </summary>
    public partial class Acceso : Window
    {
        EmpleadosRepository empleado;
        Empleado _empleado;
        public Acceso()
        {
            InitializeComponent();
            _empleado = new Empleado();
            empleado = new EmpleadosRepository(new EmpleadoValidator());
            cmbPuesto.Items.Add("Administrador");
            cmbPuesto.Items.Add("Vendedor");
        }

        private void btnEntrar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txbusuario.Text) || !string.IsNullOrEmpty(pswbContrasenia.Password) || !string.IsNullOrEmpty(cmbPuesto.SelectedItem.ToString()))
                {
                    List<Empleado> em = empleado.Read;
                    _empleado = em.Where(u => u.Nombre == txbusuario.Text).SingleOrDefault();
                    if (_empleado.Contrasenia == pswbContrasenia.Password && _empleado.Nombre == txbusuario.Text)
                    {
                        if (_empleado.TipoEmpleado == "Administrador" || _empleado.TipoEmpleado == "Vendedor")
                        {
                            ControlFarmacia empleado = new ControlFarmacia(_empleado);
                            empleado.Show();
                            this.Close();
                        }
                        else
                        {
                            throw new Exception("Seleccione un tipo de empleado");
                        }

                    }
                    else
                    {
                        throw new Exception("usuario y/o contraseña invalidos");
                    }

                }
                else
                {
                    throw new Exception("Verifique que todos los campos esten llenos");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
