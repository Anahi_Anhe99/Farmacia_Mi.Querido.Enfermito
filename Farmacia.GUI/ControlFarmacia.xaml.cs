﻿using Farmacia.BIZ;
using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Intefaces;
using Farmacia.COMMON.Validadores;
using Farmacia.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Farmacia.GUI
{
    /// <summary>
    /// Lógica de interacción para Administrador.xaml
    /// </summary>
    public partial class ControlFarmacia : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }

        ICategoriasManager manejadorCategorias;
        IClientesManager manejadorClientes;
        IEmpleadosManager manejadorEmpleados;
        IProductosAlmacenManager manejadorProductosAlmacen;
        IVentasManager manejadorVentas;

        //CategoriaValidator validadorCategoria;
        //ClienteValidator validadorCliente;
        //EmpleadoValidator validadorEmpleado;
        //ProductoValidator validadorProducto;

        accion accionCategorias;
        accion accionCliente;
        accion accionEmpleados;
        accion accionProductosAlmacen;

        Empleado _usuario;
        CategoriasRepository categoriasRepository;
        ClientesRepository clientesrepository;
        ProductosRepository productosRepository;
        VentasRepository ventasRepository;
        TicketRepository ticketRepository;
        Venta venta;

        public ControlFarmacia(Empleado usuario)
        {
            InitializeComponent();

            _usuario = usuario;

            manejadorCategorias = new CategoriasManager(new CategoriasRepository(new CategoriaValidator()));
            manejadorClientes = new ClientesManager(new ClientesRepository(new ClienteValidator()));
            manejadorEmpleados = new EmpleadosManager(new EmpleadosRepository(new EmpleadoValidator()));
            manejadorProductosAlmacen = new ProductoAlmacenManager(new ProductosRepository(new ProductoValidator()));
            manejadorVentas = new VentaManager(new VentasRepository(new VentaValidator()));

            categoriasRepository = new CategoriasRepository(new CategoriaValidator());
            clientesrepository = new ClientesRepository(new ClienteValidator());
            productosRepository = new ProductosRepository(new ProductoValidator());
            ventasRepository = new VentasRepository(new VentaValidator());
            ticketRepository = new TicketRepository();


            HabilitarBotonesCategorias(false);
            LimpiarCamposDeCategorias();
            HabilitarCajasDeCategorias(false);
            ActualizarTablaCategorias();

            HabilitarBotonesClientes(false);
            LimpiarCamposDeClientes();
            ActualizarTablaClientes();
            HabilitarCajasDeClientes(false);

            HabilitarBotonesEmpleados(false);
            LimpiarCamposDeEmpleados();
            ActualizarTablaEmpleados();
            HabilitarCajasDeEmpleados(false);
            cmbEmpleadoPuesto.Items.Add("Administrador");
            cmbEmpleadoPuesto.Items.Add("Vendedor");

            HabilitarBotonesProductosAlmacen(false);
            LimpiarCamposDeProductosAlmacen();
            ActualizarTablaProductosAlmacen();
            HabilitarCajasDeProductosAlmacen(false);
            cmbProductoCategoria.ItemsSource = categoriasRepository.Read;

            HabilitarCamposVenta(false);
            HabilitarBotonesNuevaVenta(false);
            LimpiarCamposDeNuevaVenta();
            ActualizarTablaNuevaVenta();
            txbVentaPagoEf.IsEnabled = false;
            dpkFechaHoraVenta.Text = DateTime.Now.ToString("dd-MM-yyyy  hh:mm:ss");
            txbVendedor.Text = usuario.Nombre;

            if (_usuario.TipoEmpleado == "Vendedor")
            {
                tbiCategorias.Visibility = Visibility.Hidden;
                tbiClientes.Visibility = Visibility.Hidden;
                tbiEmpleados.Visibility = Visibility.Hidden;
                tbiProductos.Visibility = Visibility.Hidden;
                tbiVentas.Visibility = Visibility.Visible;
            }
            else
            {
                tbiCategorias.Visibility = Visibility.Visible;
                tbiClientes.Visibility = Visibility.Visible;
                tbiEmpleados.Visibility = Visibility.Visible;
                tbiProductos.Visibility = Visibility.Visible;
                tbiVentas.Visibility = Visibility.Visible;
            }
        }


        private void btnsReturn_Click(object sender, RoutedEventArgs e)
        {
            Acceso principal = new Acceso();
            principal.Show();
            this.Close();
        }


        //Categorias
        private void ActualizarTablaCategorias()
        {
            dtgCategorias.ItemsSource = null;
            dtgCategorias.ItemsSource = manejadorCategorias.Listar;
        }

        private void LimpiarCamposDeCategorias()
        {
            txbCategoriaId.Text = "";
            txbCategoriaNombre.Clear();

        }

        private void HabilitarBotonesCategorias(bool value)
        {
            btnCategoriasAgregar.IsEnabled = !value;
            btnCategoriasEliminar.IsEnabled = !value;
            btnCategoriasGuardar.IsEnabled = value;
            btnCategoriasCancelar.IsEnabled = value;

        }

        private void HabilitarCajasDeCategorias(bool v)
        {
            txbCategoriaId.IsEnabled = false;
            txbCategoriaNombre.IsEnabled = v;
        }

        private void btnCategoriasAgregar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LimpiarCamposDeCategorias();
                HabilitarCajasDeCategorias(true);
                HabilitarBotonesCategorias(true);
                accionCategorias = accion.Nuevo;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia - Mi Querido Enfermito -", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCategoriasEliminar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Categoria _categoria = dtgCategorias.SelectedItem as Categoria;
                if (_categoria != null)
                {
                    if (MessageBox.Show("Confirma para eliminar la Categoría seleccionada", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (manejadorCategorias.Eliminar(_categoria.Id))
                        {
                            ActualizarTablaCategorias();
                            MessageBox.Show("Categoría Eliminada correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("Se produjo un error al intentar Eliminar el Elemento Seleccionado", "Farmacia -Mi querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No se ha seleccionado una categoría aún", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCategoriasGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (accionCategorias == accion.Nuevo)
                {
                    Categoria _categoria = new Categoria()
                    {
                        Id = txbCategoriaId.Text,
                        Nombre = txbCategoriaNombre.Text

                    };
                    if (manejadorCategorias.Agregar(_categoria))
                    {
                        LimpiarCamposDeCategorias();
                        ActualizarTablaCategorias();
                        HabilitarCajasDeCategorias(false);
                        HabilitarBotonesCategorias(false);
                        MessageBox.Show("Categoria agregada correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Verifica que el formato de los campos este llenado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    Categoria _categoria = dtgCategorias.SelectedItem as Categoria;
                    _categoria.Id = txbCategoriaId.Text;
                    _categoria.Nombre = txbCategoriaNombre.Text;

                    if (manejadorCategorias.Actualizar(_categoria))
                    {
                        LimpiarCamposDeCategorias();
                        ActualizarTablaCategorias();
                        HabilitarBotonesCategorias(false);
                        HabilitarCajasDeCategorias(false);
                        MessageBox.Show("Categoria modificada correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Verifica que el formato de los campos este llenado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCategoriasCancelar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LimpiarCamposDeCategorias();
                HabilitarBotonesCategorias(false);
                HabilitarCajasDeCategorias(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Clientes

        private void ActualizarTablaClientes()
        {
            dtgClientes.ItemsSource = null;
            dtgClientes.ItemsSource = manejadorClientes.Listar;
        }

        private void LimpiarCamposDeClientes()
        {

            txbClienteId.Text = "";
            txbClienteNombre.Clear();
            txbClienteDireccion.Clear();
            txbClienteRFC.Clear();
            txbClienteTelefono.Clear();
            txbClienteemail.Clear();
        }

        private void HabilitarBotonesClientes(bool value)
        {
            btnClientesAgregar.IsEnabled = !value;
            btnClientesEditar.IsEnabled = !value;
            btnClientesEliminar.IsEnabled = !value;
            btnClientesGuardar.IsEnabled = value;
            btnClientesCancelar.IsEnabled = value;
        }

        private void HabilitarCajasDeClientes(bool habilitada)
        {
            txbClienteId.IsEnabled = habilitada;
            txbClienteNombre.IsEnabled = habilitada;
            txbClienteDireccion.IsEnabled = habilitada;
            txbClienteRFC.IsEnabled = habilitada;
            txbClienteTelefono.IsEnabled = habilitada;
            txbClienteemail.IsEnabled = habilitada;
        }

        private void btnClientesAgregar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LimpiarCamposDeClientes();
                HabilitarBotonesClientes(true);
                HabilitarCajasDeClientes(true);
                accionCliente = accion.Nuevo;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnClientesEditar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LimpiarCamposDeClientes();
                accionCliente = accion.Editar;
                HabilitarBotonesClientes(true);
                HabilitarCajasDeClientes(true);
                Cliente _cliente = dtgClientes.SelectedItem as Cliente;
                if (_cliente != null)
                {
                    txbClienteId.Text = _cliente.Id;
                    txbClienteNombre.Text = _cliente.Nombre;
                    txbClienteDireccion.Text = _cliente.Direccion;
                    txbClienteRFC.Text = _cliente.Rfc;
                    txbClienteemail.Text = _cliente.Correo;
                    txbClienteTelefono.Text = _cliente.Telefono;
                }
                else
                {
                    MessageBox.Show("No se ha seleccionado un Cliente aún", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnClientesEliminar_Click(object sender, RoutedEventArgs e)
        {
            Cliente _cliente = dtgClientes.SelectedItem as Cliente;
            if (_cliente != null)
            {
                if (MessageBox.Show("Confirma para eliminar Cliente seleccionado", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorClientes.Eliminar(_cliente.Id))
                    {
                        ActualizarTablaClientes();
                        MessageBox.Show("Cliente eliminado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Se produjo un error al intentar Eliminar el Elemento Seleccionado", "Farmacia -Mi querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("No se ha seleccionado un Cliente aún", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnClientesGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (accionCliente == accion.Nuevo)
                {
                    Cliente _cliente = new Cliente()
                    {
                        Id = txbClienteId.Text,
                        Nombre = txbClienteNombre.Text,
                        Direccion = txbClienteDireccion.Text,
                        Rfc = txbClienteRFC.Text.ToUpper(),
                        Telefono = txbClienteTelefono.Text,
                        Correo = txbClienteemail.Text
                    };
                    if (manejadorClientes.Agregar(_cliente))
                    {
                        LimpiarCamposDeClientes();
                        ActualizarTablaClientes();
                        HabilitarBotonesClientes(false);
                        HabilitarCajasDeClientes(false);
                        MessageBox.Show("Cliente agregado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Se produjo un error al intentar Agregar el Elemento", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    Cliente _cliente = dtgClientes.SelectedItem as Cliente;
                    _cliente.Id = txbClienteId.Text;
                    _cliente.Nombre = txbClienteNombre.Text;
                    _cliente.Direccion = txbClienteDireccion.Text;
                    _cliente.Rfc = txbClienteRFC.Text;
                    _cliente.Telefono =txbClienteTelefono.Text;
                    _cliente.Correo = txbClienteemail.Text;
                    if (manejadorClientes.Actualizar(_cliente))
                    {
                        LimpiarCamposDeClientes();
                        ActualizarTablaClientes();
                        HabilitarBotonesClientes(false);
                        HabilitarCajasDeClientes(false);
                        MessageBox.Show("Cliente modificado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Verifica que el formato de los campos este llenado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnClientesCancelar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LimpiarCamposDeClientes();
                HabilitarBotonesClientes(false);
                HabilitarCajasDeClientes(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Empleados

        private void ActualizarTablaEmpleados()
        {
            dtgEmpleados.ItemsSource = null;
            dtgEmpleados.ItemsSource = manejadorEmpleados.Listar;
        }

        private void LimpiarCamposDeEmpleados()
        {
            txbEmpleadoId.Text = "";
            txbEmpleadoNombre.Clear();
            pswEmpleadoContrasenia.Clear();
            cmbEmpleadoPuesto.SelectedItem = null;
        }

        private void HabilitarBotonesEmpleados(bool value)
        {
            btnEmpleadoNuevo.IsEnabled = !value;
            btnEmpleadoEditar.IsEnabled = !value;
            btnEmpleadoEliminar.IsEnabled = !value;
            btnEmpleadoGuardar.IsEnabled = value;
            btnEmpleadoCancelar.IsEnabled = value;
        }

        private void HabilitarCajasDeEmpleados(bool habilitada)
        {
            txbClienteemail.IsEnabled = habilitada;
            txbEmpleadoId.IsEnabled = habilitada;
            txbEmpleadoNombre.IsEnabled = habilitada;
            pswEmpleadoContrasenia.IsEnabled = habilitada;
            cmbEmpleadoPuesto.IsEnabled = habilitada;
        }

        private void btnEmpleadoNuevo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LimpiarCamposDeEmpleados();
                HabilitarBotonesEmpleados(true);
                HabilitarCajasDeEmpleados(true);
                accionEmpleados = accion.Nuevo;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void btnEmpleadoEditar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LimpiarCamposDeEmpleados();
                accionEmpleados = accion.Editar;
                HabilitarBotonesEmpleados(true);
                HabilitarCajasDeEmpleados(true);
                Empleado _empleado = dtgEmpleados.SelectedItem as Empleado;
                if (_empleado != null)
                {
                    txbEmpleadoId.Text = _empleado.Id;
                    txbEmpleadoNombre.Text = _empleado.Nombre;
                    cmbEmpleadoPuesto.Text = _empleado.TipoEmpleado;
                    pswEmpleadoContrasenia.Password = _empleado.Contrasenia;
                }
                else
                {
                    MessageBox.Show("No se ha seleccionado un Empleado aún", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia - Mi Querido Enfermito -", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnEmpleadoEliminar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Empleado _empleado = dtgEmpleados.SelectedItem as Empleado;
                if (_empleado != null)
                {
                    if (MessageBox.Show("Confirma para eliminar Empleado seleccionado", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (manejadorEmpleados.Eliminar(_empleado.Id))
                        {
                            ActualizarTablaEmpleados();
                            MessageBox.Show("Empleado eliminado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("Se produjo un error al intentar Eliminar el Elemento Seleccionado", "Farmacia -Mi querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No se ha seleccionado un Empleado aún", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnEmpleadoGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (accionEmpleados == accion.Nuevo)
                {
                    Empleado _empleado = new Empleado()
                    {
                        Id = txbEmpleadoId.Text,
                        Nombre = txbEmpleadoNombre.Text,
                        TipoEmpleado = cmbEmpleadoPuesto.Text,
                        Contrasenia = pswEmpleadoContrasenia.Password
                    };
                    if (manejadorEmpleados.Agregar(_empleado))
                    {
                        LimpiarCamposDeEmpleados();
                        ActualizarTablaEmpleados();
                        HabilitarBotonesEmpleados(false);
                        MessageBox.Show("Empleado agregado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Verifica que el formato de los campos este llenado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    Empleado _empleado = dtgEmpleados.SelectedItem as Empleado;
                    _empleado.Id = txbEmpleadoId.Text;
                    _empleado.Nombre = txbEmpleadoNombre.Text;
                    _empleado.TipoEmpleado = cmbEmpleadoPuesto.Text;
                    _empleado.Contrasenia = pswEmpleadoContrasenia.Password;
                    if (manejadorEmpleados.Actualizar(_empleado))
                    {
                        LimpiarCamposDeEmpleados();
                        ActualizarTablaEmpleados();
                        HabilitarBotonesEmpleados(false);
                        HabilitarCajasDeEmpleados(false);
                        MessageBox.Show("Empleado modificado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Verifica que el formato de los campos este llenado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnEmpleadoCancelar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LimpiarCamposDeEmpleados();
                HabilitarBotonesEmpleados(false);
                HabilitarCajasDeEmpleados(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //Productos

        private void ActualizarTablaProductosAlmacen()
        {
            dtgProductos.ItemsSource = null;
            dtgProductos.ItemsSource = manejadorProductosAlmacen.Listar;
        }

        private void LimpiarCamposDeProductosAlmacen()
        {
            txbProductoId.Text = "";
            txbProductoNombre.Clear();
            txbProductoPresentacion.Clear();
            txbProductoDescripcion.Clear();
            cmbProductoCategoria.SelectedItem = null;
            dudProductoCompra.Text = "";
            dudProductoVenta.Text = "";
        }

        private void HabilitarBotonesProductosAlmacen(bool value)
        {
            btnProductosNuevo.IsEnabled = !value;
            brnProductosEditar.IsEnabled = !value;
            btnProductosBorrar.IsEnabled = !value;
            btnProductosGuardar.IsEnabled = value;
            btnProductosCancelar.IsEnabled = value;
        }

        private void HabilitarCajasDeProductosAlmacen(bool habilitada)
        {
            txbProductoId.IsEnabled = habilitada;
            txbProductoNombre.IsEnabled = habilitada;
            txbProductoPresentacion.IsEnabled = habilitada;
            txbProductoDescripcion.IsEnabled = habilitada;
            cmbProductoCategoria.IsEnabled = habilitada;
            dudProductoCompra.IsEnabled = habilitada;
            dudProductoVenta.IsEnabled = habilitada;
        }

        private void btnProductosNuevo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LimpiarCamposDeProductosAlmacen();
                HabilitarBotonesProductosAlmacen(true);
                HabilitarCajasDeProductosAlmacen(true);
                accionProductosAlmacen = accion.Nuevo;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void brnProductosEditar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LimpiarCamposDeProductosAlmacen();
                accionProductosAlmacen = accion.Editar;
                HabilitarCajasDeProductosAlmacen(true);
                HabilitarBotonesProductosAlmacen(true);
                ProductoAlmacen _pAlmacen = dtgProductos.SelectedItem as ProductoAlmacen;
                if (_pAlmacen != null)
                {
                    txbProductoId.Text = _pAlmacen.Id;
                    txbProductoNombre.Text = _pAlmacen.Nombre;
                    txbProductoPresentacion.Text = _pAlmacen.Presentacion;
                    txbProductoDescripcion.Text = _pAlmacen.Descripcion;
                    cmbProductoCategoria.SelectedItem = _pAlmacen.Categoria;
                    dudProductoCompra.Text = Convert.ToString(_pAlmacen.PrecioCompra);
                    dudProductoVenta.Text = Convert.ToString(_pAlmacen.PrecioVenta);
                }
                else
                {
                    MessageBox.Show("Aún no se ha seleccionado un Producto del Almacén ", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia - Mi Querido Enfermito -", MessageBoxButton.OK, MessageBoxImage.Error);
            }



        }

        private void btnProductosBorrar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProductoAlmacen _pAlmacen = dtgProductos.SelectedItem as ProductoAlmacen;
                if (_pAlmacen != null)
                {
                    if (MessageBox.Show("Confirma para eliminar Producto seleccionado", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (manejadorProductosAlmacen.Eliminar(_pAlmacen.Id))
                        {
                            ActualizarTablaProductosAlmacen();
                            MessageBox.Show("Producto eliminado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("Se produjo un error al intentar Eliminar el Elemento Seleccionado", "Farmacia -Mi querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No se ha seleccionado un Producto aún", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnProductosGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (accionProductosAlmacen == accion.Nuevo)
                {
                    ProductoAlmacen _pAlmacen = new ProductoAlmacen()
                    {
                        Id = txbProductoId.Text,
                        Nombre = txbProductoNombre.Text,
                        Presentacion = txbProductoPresentacion.Text,
                        Descripcion = txbProductoDescripcion.Text,
                        Categoria = cmbProductoCategoria.Text,
                        PrecioCompra = float.Parse(dudProductoCompra.Text),
                        PrecioVenta = float.Parse(dudProductoVenta.Text),
                    };
                    if (manejadorProductosAlmacen.Agregar(_pAlmacen))
                    {
                        LimpiarCamposDeProductosAlmacen();
                        ActualizarTablaProductosAlmacen();
                        HabilitarBotonesProductosAlmacen(false);
                        HabilitarCajasDeProductosAlmacen(false);
                        MessageBox.Show("Producto agregado correctamente al Almacén", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Verifica que el formato de los campos este llenado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    ProductoAlmacen _pAlmacen = dtgProductos.SelectedItem as ProductoAlmacen;
                    _pAlmacen.Id = txbProductoId.Text;
                    _pAlmacen.Nombre = txbProductoNombre.Text;
                    _pAlmacen.Presentacion = txbProductoPresentacion.Text;
                    _pAlmacen.Descripcion = txbProductoDescripcion.Text;
                    _pAlmacen.Categoria = cmbProductoCategoria.Text;
                    _pAlmacen.PrecioCompra = float.Parse(dudProductoCompra.Text);
                    _pAlmacen.PrecioVenta = float.Parse(dudProductoVenta.Text);


                    if (manejadorProductosAlmacen.Actualizar(_pAlmacen))
                    {
                        LimpiarCamposDeProductosAlmacen();
                        ActualizarTablaProductosAlmacen();
                        HabilitarBotonesProductosAlmacen(false);
                        HabilitarCajasDeProductosAlmacen(false);
                        MessageBox.Show("Producto modificado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Hubo un  error al actualizar el Producto", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }


        }

        private void btnProductosCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEmpleados();
            HabilitarBotonesProductosAlmacen(false);
            HabilitarCajasDeProductosAlmacen(false);
        }

        //Nueva Venta

        private void HabilitarCamposVenta(bool v)
        {
            cmbVentaCliente.IsEnabled = v;
            cmbVentaProducto.IsEnabled = v;
        }

        private void ActualizarTablaNuevaVenta()
        {
            dtgProductosVendidos.ItemsSource = null;
            dtgProductosVendidos.ItemsSource = manejadorVentas.Listar;
        }

        private void ActualizarListaDeProductosVenta()
        {
            dtgProductosVendidos.ItemsSource = null;
            dtgProductosVendidos.ItemsSource = venta.ProductoVenta;
        }

        private void LimpiarCamposDeNuevaVenta()
        {
            cmbVentaCliente.SelectedItem = null;
            cmbVentaProducto.SelectedItem = null;
            txbVentaIva.Clear();
            txbVentaTotal.Clear();
            txbVentaPagoEf.Clear();
            txbVentaCambio.Clear();
        }

        private void HabilitarBotonesNuevaVenta(bool v)
        {
            btnNuevaVenta.IsEnabled = !v;
            btnVentaAgregarProducto.IsEnabled = v;
            btnVentaQuitarProducto.IsEnabled = v;
            btnGenerarVenta.IsEnabled = v;
        }

        //Operaciones
        float total;

        private float CalcularIva(float total)
        {
            return ((total * 16) / 100);
        }

        private float CalcularTotal(List<ProductoAlmacen> productoVenta)
        {
            foreach (var item in productoVenta)
            {
                total = total + item.PrecioVenta;
            }
            return total;
        }

        private object GenerarCambio(float total, float pagoEfectivo)
        {
            return pagoEfectivo - total;
        }
        //

        private void btnNuevaVenta_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                HabilitarCamposVenta(true);
                HabilitarBotonesNuevaVenta(true);
                LimpiarCamposDeNuevaVenta();
                cmbVentaCliente.ItemsSource = clientesrepository.Read;
                cmbVentaProducto.ItemsSource = productosRepository.Read;
                venta = new Venta();
                venta.ProductoVenta = new List<ProductoAlmacen>();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnVentaAgregarProducto_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                total = 0;

                txbVentaPagoEf.IsEnabled = true;
                ProductoAlmacen m = cmbVentaProducto.SelectedItem as ProductoAlmacen;
                if (m != null)
                {
                    venta.ProductoVenta.Add(m);
                    ActualizarListaDeProductosVenta();
                }
                txbVentaTotal.Text = Convert.ToString(CalcularTotal(venta.ProductoVenta));
                txbVentaIva.Text = Convert.ToString(CalcularIva(float.Parse(txbVentaTotal.Text)));
                cmbVentaProducto.SelectedItem = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnVentaQuitarProducto_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                total = 0;
                venta.ProductoVenta.Remove(dtgProductosVendidos.SelectedItem as ProductoAlmacen);
                ActualizarListaDeProductosVenta();
                txbVentaTotal.Text = Convert.ToString(CalcularTotal(venta.ProductoVenta));
                txbVentaIva.Text = Convert.ToString(CalcularIva(float.Parse(txbVentaTotal.Text)));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnGenerarVenta_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txbVentaPagoEf.Text))
                {
                    MessageBox.Show("Favor de ingresar El monto en efectivo", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                venta.Total = float.Parse(txbVentaTotal.Text);
                venta.PagoEfectivo = float.Parse(txbVentaPagoEf.Text);

                if (venta.PagoEfectivo < venta.Total)
                {
                    MessageBox.Show("Lo siento, monto a pagar por debajo del total de la venta", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                venta.ProductoVenta = venta.ProductoVenta;
                venta.Empleado = txbVendedor.Text; ;
                venta.Cliente = cmbVentaCliente.Text;
                venta.Iva = float.Parse(txbVentaIva.Text);
                venta.Cambio = float.Parse(txbVentaCambio.Text = Convert.ToString(GenerarCambio(venta.Total, venta.PagoEfectivo)));
                if (ticketRepository.Create(venta))
                {
                    MessageBox.Show("Venta Generada con éxito", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    HabilitarCamposVenta(false);
                    HabilitarBotonesNuevaVenta(false);
                    LimpiarCamposDeNuevaVenta();
                    ActualizarListaDeProductosVenta();
                    dtgProductosVendidos.ItemsSource = null;
                }
                else
                {
                    MessageBox.Show("Verifica que el formato de los campos este llenado correctamente", "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Farmacia -Mi Querido Enfermito-", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


    }
}
