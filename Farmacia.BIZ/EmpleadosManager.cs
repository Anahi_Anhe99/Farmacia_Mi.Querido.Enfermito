﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.BIZ
{
    public class EmpleadosManager : IEmpleadosManager
    {
        IGenericRepository<Empleado> repositorio;
        public EmpleadosManager(IGenericRepository<Empleado> repositorio)
        {
            this.repositorio = repositorio;
        }

        public bool Agregar(Empleado entidad)
        {
            return repositorio.Create(entidad);
        }

        public List<Empleado> Listar => repositorio.Read;

        public bool Actualizar(Empleado entidad)
        {
            return repositorio.Update(entidad);
        }
        
        public bool Eliminar(string id)
        {
            return repositorio.Delete(id);
        }

        public Empleado BuscarPorId(string id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }
    }
}
