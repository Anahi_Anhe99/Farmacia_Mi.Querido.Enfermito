﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.BIZ
{
    public class CategoriasManager : ICategoriasManager
    {
        IGenericRepository<Categoria> repositorio;
        public CategoriasManager(IGenericRepository<Categoria> repositorio)
        {
            this.repositorio = repositorio;
        }


        public bool Agregar(Categoria entidad)
        {
            return repositorio.Create(entidad);
        }

        public List<Categoria> Listar => repositorio.Read;

        public bool Actualizar(Categoria entidad)
        {
            return repositorio.Update(entidad);
        }
        
        public bool Eliminar(string id)
        {
            return repositorio.Delete(id);
        }
        public Categoria BuscarPorId(string id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }
    }
}
