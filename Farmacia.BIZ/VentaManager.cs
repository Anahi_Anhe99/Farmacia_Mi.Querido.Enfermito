﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.BIZ
{
    public class VentaManager : IVentasManager
    {
        IGenericRepository<Venta> repositorio;
        public VentaManager(IGenericRepository<Venta> repositorio)
        {
            this.repositorio = repositorio;
        }

        public bool Agregar(Venta entidad)
        {
            return repositorio.Create(entidad);
        }

        public List<Venta> Listar => repositorio.Read;

        public bool Actualizar(Venta entidad)
        {
            return repositorio.Update(entidad);
        }

        public bool Eliminar(string id)
        {
            return repositorio.Delete(id);
        }

        public Venta BuscarPorId(string id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }
    }
}
