﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.BIZ
{
    public class ClientesManager : IClientesManager
    {
        IGenericRepository<Cliente> repositorio;
        public ClientesManager(IGenericRepository<Cliente> repositorio)
        {
            this.repositorio = repositorio;
        }

        public bool Agregar(Cliente entidad)
        {
            return repositorio.Create(entidad);
        }

        public List<Cliente> Listar => repositorio.Read;

        public bool Actualizar(Cliente entidad)
        {
            return repositorio.Update(entidad);
        }

        public bool Eliminar(string id)
        {
            return repositorio.Delete(id);
        }

        public Cliente BuscarPorId(string id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }
    }
}
