﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.BIZ
{
    public class ProductoAlmacenManager : IProductosAlmacenManager
    {
        IGenericRepository<ProductoAlmacen> repositorio;
        public ProductoAlmacenManager(IGenericRepository<ProductoAlmacen> repositorio)
        {
            this.repositorio = repositorio;
        }

        public bool Agregar(ProductoAlmacen entidad)
        {
            return repositorio.Create(entidad);
        }

        public List<ProductoAlmacen> Listar => repositorio.Read;

        public bool Actualizar(ProductoAlmacen entidad)
        {
            return repositorio.Update(entidad);
        }

        public ProductoAlmacen BuscarPorId(string id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return repositorio.Delete(id);
        }
    }
}
