﻿using Farmacia.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Intefaces
{
    public interface IGenericManager<T> where T : BaseDTO
    {
        bool Agregar(T entidad);
        List<T> Listar { get; }
        bool Eliminar(string id);
        bool Actualizar(T entidad);
        T BuscarPorId(string id);

    }
}
