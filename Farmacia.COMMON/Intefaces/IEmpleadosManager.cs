﻿using Farmacia.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Intefaces
{
    public interface IEmpleadosManager:IGenericManager<Empleado>
    {
    }
}
