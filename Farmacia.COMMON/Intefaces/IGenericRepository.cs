﻿using Farmacia.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Intefaces
{
    public interface IGenericRepository<T> where T:BaseDTO
    {
        bool Create(T entidad);
        List<T> Read { get; }
        bool Update(T EntidadModificada);
        bool Delete(string id);
    }
}
