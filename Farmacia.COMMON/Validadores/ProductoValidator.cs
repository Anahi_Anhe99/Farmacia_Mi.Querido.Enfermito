﻿using Farmacia.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Validadores
{
    public class ProductoValidator:GenericValidator<ProductoAlmacen>
    {
        public ProductoValidator()
        {
            RuleFor(e => e.Descripcion).NotNull().NotEmpty().MaximumLength(20);
            RuleFor(e => e.Presentacion).NotNull().NotEmpty().MaximumLength(15);
            RuleFor(e => e.Categoria).NotNull().NotEmpty();
            RuleFor(e => e.PrecioCompra).NotNull().NotEmpty();
            RuleFor(e => e.PrecioVenta).NotNull().NotEmpty();
        }
    }
}
