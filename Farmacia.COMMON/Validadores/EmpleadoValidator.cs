﻿using Farmacia.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Validadores
{
    public class EmpleadoValidator:GenericValidator<Empleado>
    {
        public EmpleadoValidator()
        {
            RuleFor(e => e.Contrasenia).NotNull().NotEmpty().MinimumLength(8);
            RuleFor(e => e.TipoEmpleado).NotNull().NotEmpty();
        }
    }
}
