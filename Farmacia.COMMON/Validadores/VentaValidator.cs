﻿using Farmacia.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Validadores
{
    public class VentaValidator:GenericValidator<Venta>
    {
        public VentaValidator()
        {
            RuleFor(e => e.Empleado).NotNull().NotEmpty();
            RuleFor(e => e.Cliente).NotNull().NotEmpty();
            RuleFor(e => e.ProductoVenta).NotNull().NotEmpty();
            RuleFor(e => e.Iva).NotNull().NotEmpty();
            RuleFor(e => e.Total).NotNull().NotEmpty();
            RuleFor(e => e.FechaHora).NotNull().NotEmpty();
            RuleFor(e => e.PagoEfectivo).NotNull().NotEmpty();
            RuleFor(e => e.Cambio).NotNull().NotEmpty();
        }
    }
}
