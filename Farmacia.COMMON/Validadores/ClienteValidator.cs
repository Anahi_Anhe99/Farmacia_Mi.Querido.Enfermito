﻿using Farmacia.COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Validadores
{
    public class ClienteValidator : GenericValidator<Cliente>
    {
        public ClienteValidator()
        {
            RuleFor(e => e.Direccion).NotNull().NotEmpty().MaximumLength(50);
            RuleFor(e => e.Rfc).NotNull().NotEmpty().MinimumLength(12).MaximumLength(13);
            RuleFor(e => e.Correo).EmailAddress();
            RuleFor(e => e.Telefono).Length(10).NotNull().NotEmpty();
        }
    }
}
