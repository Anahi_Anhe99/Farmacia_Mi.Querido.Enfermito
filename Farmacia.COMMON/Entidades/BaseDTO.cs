﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Entidades
{
    public abstract class BaseDTO
    {
        public string Id { get; set; }
        public string Nombre { get; set; }

    }
}
