﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Entidades
{
    public class Cliente:BaseDTO
    {
        public string Direccion { get; set; }
        public string Rfc { get; set; }
        public string Telefono { get; set; }
        public string Correo  { get; set; }
        public override string ToString()
        {
            return string.Format("{0}", Nombre);
        }
    }
}
