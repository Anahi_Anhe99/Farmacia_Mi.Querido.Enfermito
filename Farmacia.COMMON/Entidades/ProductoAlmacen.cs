﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Entidades
{
    public class ProductoAlmacen:BaseDTO
    {
        public string Descripcion { get; set; }
        public string Presentacion { get; set; }
        public string Categoria { get; set; }
        public float PrecioCompra { get; set; }
        public float PrecioVenta { get; set; }
        public override string ToString()
        {
            return string.Format("{0} | {1}", Nombre, Presentacion);
        }
    }
}
