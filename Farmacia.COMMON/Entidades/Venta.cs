﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia.COMMON.Entidades
{
    public class Venta:BaseDTO
    {
        public string Empleado { get; set; }
        public string Cliente { get; set; }
        public List<ProductoAlmacen> ProductoVenta { get; set; }
        public float Iva { get; set; }
        public float Total { get; set; }
        public DateTime FechaHora { get; set; }
        public float PagoEfectivo { get; set; }
        public float Cambio { get; set; }

    }
}
