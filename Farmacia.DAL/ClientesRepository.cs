﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Intefaces;
using Farmacia.COMMON.Validadores;
using FluentValidation.Results;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.DAL
{
    public class ClientesRepository : IGenericRepository<Cliente>
    {
        private string DBName = @"C:/Mi querido Enfermito/Farmacia.db";
        private string TableName = "Clientes";
        private ClienteValidator Validator;
        private ValidationResult ResultadoValidacion;
        public ClientesRepository(ClienteValidator validator)
        {
            Validator = validator;
        }

        public List<Cliente>Read
        {
            get
            {
                List<Cliente> datos = new List<Cliente>();
                using (var db = new LiteDatabase(DBName))
                {
                    datos = db.GetCollection<Cliente>(TableName).FindAll().ToList();
                }
                return datos;
            }
        }


        public bool Create(Cliente entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            ResultadoValidacion = Validator.Validate(entidad);
            try
            {
                if (ResultadoValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var coleccion = db.GetCollection<Cliente>(TableName);
                        coleccion.Insert(entidad);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Cliente>(TableName);
                    r = coleccion.Delete(e => e.Id == id);
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool Update(Cliente EntidadModificada)
        {
            try
            {
                ResultadoValidacion = Validator.Validate(EntidadModificada);
                if (ResultadoValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var coleccion = db.GetCollection<Cliente>(TableName);
                        coleccion.Update(EntidadModificada);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
