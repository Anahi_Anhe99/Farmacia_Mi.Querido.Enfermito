﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Intefaces;
using Farmacia.COMMON.Validadores;
using FluentValidation.Results;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.DAL
{
    public class ProductosRepository : IGenericRepository<ProductoAlmacen>
    {
        private string DBName = @"C:/Mi querido Enfermito/Farmacia.db";
        private string TableName = "ProductosAlmacen";
        private ProductoValidator Validator;
        private ValidationResult ResultValidation;
        public ProductosRepository(ProductoValidator validator)
        {
            Validator = validator;
        }
        public List<ProductoAlmacen> Read
        {
            get
            {
                List<ProductoAlmacen> datos = new List<ProductoAlmacen>();
                using (var db = new LiteDatabase(DBName))
                {
                    datos = db.GetCollection<ProductoAlmacen>(TableName).FindAll().ToList();
                }
                return datos;
            }
        }

        public bool Create(ProductoAlmacen entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            ResultValidation = Validator.Validate(entidad);
            try
            {
                if (ResultValidation.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var coleccion = db.GetCollection<ProductoAlmacen>(TableName);
                        coleccion.Insert(entidad);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<ProductoAlmacen>(TableName);
                    r = coleccion.Delete(e => e.Id == id);
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(ProductoAlmacen EntidadModificada)
        {
            try
            {
                ResultValidation = Validator.Validate(EntidadModificada);
                if (ResultValidation.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var coleccion = db.GetCollection<ProductoAlmacen>(TableName);
                        coleccion.Update(EntidadModificada);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
