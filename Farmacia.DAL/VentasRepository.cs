﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Intefaces;
using Farmacia.COMMON.Validadores;
using FluentValidation.Results;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.DAL
{
    public class VentasRepository : IGenericRepository<Venta>
    {
        private string DBName = @"C:/Mi querido Enfermito/Farmacia.db";
        private string TableName = "Ventas";
        private VentaValidator Validator;
        private ValidationResult ResultValidation;
        public VentasRepository(VentaValidator validator)
        {
            Validator = validator;
        }
        public List<Venta> Read
        {
            get
            {
                List<Venta> datos = new List<Venta>();
                using (var db = new LiteDatabase(DBName))
                {
                    datos = db.GetCollection<Venta>(TableName).FindAll().ToList();
                }
                return datos;
            }
        }

        public bool Create(Venta entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            ResultValidation = Validator.Validate(entidad);
            try
            {
                if (ResultValidation.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var coleccion = db.GetCollection<Venta>(TableName);
                        coleccion.Insert(entidad);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Venta>(TableName);
                    r = coleccion.Delete(e => e.Id == id);
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Venta EntidadModificada)
        {
            try
            {
                ResultValidation = Validator.Validate(EntidadModificada);
                if (ResultValidation.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var coleccion = db.GetCollection<Venta>(TableName);
                        coleccion.Update(EntidadModificada);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
