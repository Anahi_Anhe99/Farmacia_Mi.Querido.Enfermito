﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Intefaces;
using LiteDB;
using System;
using System.Collections.Generic;
using FluentValidation.Results;
using System.Linq;
using System.Text;
using FluentValidation;
using Farmacia.COMMON.Validadores;

namespace Farmacia.DAL
{
    public class CategoriasRepository : IGenericRepository<Categoria>
    {
        private string DBName = @"C:/Mi querido Enfermito/Farmacia.db";
        private string TableName = "Categorias";
        private CategoriaValidator Validator;
        private ValidationResult ResultadoValidacion;
        public CategoriasRepository(CategoriaValidator validator)
        {
            Validator = validator;
        }
        public List<Categoria> Read
        {
            get
            {
                List<Categoria> datos = new List<Categoria>();
                using (var db = new LiteDatabase(DBName))
                {
                    datos = db.GetCollection<Categoria>(TableName).FindAll().ToList();
                }
                return datos;
            }
        }

        public bool Create(Categoria entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            ResultadoValidacion = Validator.Validate(entidad);

            try
            {
                if (ResultadoValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var coleccion = db.GetCollection<Categoria>(TableName);
                        coleccion.Insert(entidad);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Categoria>(TableName);
                    r = coleccion.Delete(e => e.Id == id);
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Categoria EntidadModificada)
        {
            throw new NotImplementedException();
        }
    }

}

