﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Herramientas;
using Farmacia.COMMON.Intefaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Farmacia.DAL
{
    public class TicketRepository : IGenericRepository<Venta>
    {
        ManejadorArchivos archivo;
        Venta ticket;
        List<ProductoAlmacen>ProductoVenta;
        string nombrearchivo;
        public TicketRepository()
        {
            nombrearchivo = DateTime.Now.ToString("dd-MM-yy hh_mm_ss");
            archivo = new ManejadorArchivos($@"C:/Mi querido Enfermito/Ticket.{nombrearchivo}.txt");
            ProductoVenta= new List<ProductoAlmacen>();
            ticket = new Venta();
        }

        public bool Create(Venta entidad)
        {
            ticket.ProductoVenta = entidad.ProductoVenta;
            ticket.Empleado = entidad.Empleado;
            ticket.Cliente = entidad.Cliente;
            ticket.Iva = entidad.Iva;
            ticket.Total = entidad.Total;
            ticket.PagoEfectivo = entidad.PagoEfectivo;
            ticket.Cambio = entidad.Cambio;
            ProductoVenta = entidad.ProductoVenta;
            bool _resultado = ActualizarArchivo();
            return _resultado;
        }

        private bool ActualizarArchivo()
        {
            string contenido = null;
            contenido = $"\n       -FARMACIAS MI QUERIDO ENFERMITO SA DE CV-\r\rSUCURSAL HUICHAPAN\nCalle Jorge Rojo Lugo\nTel: 7731097482\nE-mail:miqueridoe@gmail.com\n\nFecha: {nombrearchivo}\n\nCliente: {ticket.Cliente}\nAtendio: {ticket.Empleado}\n\n _____________________________________________\n";
            foreach (var item in ProductoVenta)
            {
                contenido += string.Format("1\t|\t{0}\t|\t${1}\n", item.Nombre, item.PrecioVenta);
            }
            contenido += $"\n\nIVA: {ticket.Iva}\nTotal: ${ticket.Total}\nPago Efectivo: ${ticket.PagoEfectivo}\nCambio: {ticket.Cambio}\n_____________________________________________\n     FUE UN PLACER ATENDERLE";
            return archivo.Guardar(contenido);
        }

        public List<Venta> Read => throw new NotImplementedException();

        public bool Delete(string id)
        {
            throw new NotImplementedException();
        }

        public bool Update(Venta EntidadModificada)
        {
            throw new NotImplementedException();
        }
    }
}
