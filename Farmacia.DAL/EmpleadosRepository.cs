﻿using Farmacia.COMMON.Entidades;
using Farmacia.COMMON.Intefaces;
using Farmacia.COMMON.Validadores;
using FluentValidation.Results;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Farmacia.DAL
{
    public class EmpleadosRepository : IGenericRepository<Empleado>
    {
        private string DBName = @"C:/Mi querido Enfermito/Farmacia.db";
        private string TableName = "Empleados";
        private EmpleadoValidator Validator;
        private ValidationResult ResultadoValidacion;
        public EmpleadosRepository(EmpleadoValidator validator)
        {
            Validator = validator;
        }
        public List<Empleado> Read
        {
            get
            {
                List<Empleado> datos = new List<Empleado>();
                using (var db = new LiteDatabase(DBName))
                {
                    datos = db.GetCollection<Empleado>(TableName).FindAll().ToList();
                }
                return datos;
            }
        }

        public bool Create(Empleado entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            ResultadoValidacion = Validator.Validate(entidad);
            try
            {
                if (ResultadoValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var coleccion = db.GetCollection<Empleado>(TableName);
                        coleccion.Insert(entidad);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Empleado>(TableName);
                    r = coleccion.Delete(e => e.Id == id);
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Empleado EntidadModificada)
        {
            try
            {
                ResultadoValidacion = Validator.Validate(EntidadModificada);
                if (ResultadoValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var coleccion = db.GetCollection<Empleado>(TableName);
                        coleccion.Update(EntidadModificada);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
